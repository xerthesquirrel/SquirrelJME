// -*- Mode: Java; indent-tabs-mode: t; tab-width: 4 -*-
// ---------------------------------------------------------------------------
// Multi-Phasic Applications: SquirrelJME
//     Copyright (C) Stephanie Gawroriski <xer@multiphasicapps.net>
// ---------------------------------------------------------------------------
// SquirrelJME is under the Mozilla Public License Version 2.0.
// See license.mkd for licensing and copyright information.
// ---------------------------------------------------------------------------

/**
 * Additional 3D Graphics APIs.
 *
 * @since 2024/11/03
 */

@cc.squirreljme.runtime.cldc.annotation.SquirrelJMEVendorApi
package com.nttdocomo.ui.graphics3d;
